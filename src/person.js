class Person {
    constructor(name) {
        this.name = name;
    }

    walk() {
        console.log(this.name + ' walks...');
    }
}

export default Person;