import Teacher, { promote } from "./teacher";
import React, { Component } from 'react';

const teacher = new Teacher('Tobi', 'BSc');
teacher.walk();
teacher.teach();
console.log(teacher.degree);